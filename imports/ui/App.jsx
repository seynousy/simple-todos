import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { createContainer } from 'meteor/react-meteor-data';
import { Meteor } from 'meteor/meteor'
import { Tasks } from '../api/tasks.js'

import Task from './Task.jsx'
import AccountsUiWrapper from './AccountsUiWrapper.jsx'

class App extends Component {
	constructor(props) {
		super(props);
		this.state = {
			newTask: "",
			hideCompleted: false,
		}
	}

	renderTasks() {
		let filteredTasks = this.props.tasks
		if (this.state.hideCompleted) {
			filteredTasks = filteredTasks.filter(task => !task.checked)
		}
		return filteredTasks.map((task) => {
			const currentUserId = this.props.currentUser && this.props.currentUser._id
			const showPrivateButton = task.owner === currentUserId

			return(
				<Task
					key={task._id} 
					task={task}
					showPrivateButton={showPrivateButton}
				/>
			)
		})
	}

	handleNewTaskChange(e) {
		this.setState({
			newTask: e.target.value
		})
	}

	handleSubmit(e) {
		e.preventDefault()
		
		Meteor.call('tasks.insert', this.state.newTask)
	}

	toggleHideCompleted() {
		this.setState({
			hideCompleted: !this.state.hideCompleted
		})
	}

	render() {
		return(
			<div className="container">
				<header>
					<AccountsUiWrapper />
					<h1>Common Todo List</h1>
					<h4>({this.props.incompletedTasks}) common incompleted tasks</h4>
					{ Meteor.userId() ? 
						<h4>({this.props.incompletedOwnTasks}) own incompleted tasks</h4>
						: ''
					}
					<label className="hide-completed">
						<input
							type="checkbox"
							readOnly
							checked={this.state.hideCompleted}
							onClick={this.toggleHideCompleted.bind(this)}
						/>
						Hide completed Tasks
					</label>
					{ this.props.currentUser ?
						<form className="new-task" onSubmit={this.handleSubmit.bind(this)}>
							<input
								type="text"
								name="text"
								placeholder="Type to add new tasks"
								onChange={this.handleNewTaskChange.bind(this)}
							/>
						</form> : ''
					}
				</header>
				<ul>
					{this.renderTasks()}
				</ul>
			</div>
		)
	}
}

App.propTypes = {
	tasks: PropTypes.array.isRequired,
	incompletedTasks: PropTypes.number.isRequired,
	incompletedOwnTasks: PropTypes.number.isRequired,
};

export default createContainer(() => {
	Meteor.subscribe('tasks')
	return {
		tasks: Tasks.find({}, { sort: { createdAt: -1 } }).fetch(),
		incompletedTasks: Tasks.find({ checked: { $ne: true } }).count(),
		incompletedOwnTasks: Tasks.find({ checked: { $ne: true }, owner: { $eq: Meteor.userId() } }).count(),
		currentUser: Meteor.user(),
	};
}, App);
